package com.qaagility.controller;

import org.junit.Test;
import static org.junit.Assert.*;

public class AboutTest {

    @Test
    public void testdesc() throws Exception {

        String k= new About().desc();
        assertEquals("This application was copied from somewhere, sorry but I cannot give the details and the proper copyright notice. Please don't tell the police!", 1, k);
    }
} 
